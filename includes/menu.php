<nav class="navbar navbar-expand-lg navbar-dark bg-transparent">
        <div class="container-fluid">
            <a class="navbar-brand" href="#home">
                <img src="assets/images/logo.png" class="logo" alt="Fisgar" title="Fisgar" />
                <img src="assets/images/logo-c.png" class="logo-c d-none" alt="Fisgar" title="Fisgar" />
            </a>
            <div class="navbar-collapse" id="conteudoNavbarSuportado">
                <div class="mr-auto">
                    <ul class="navbar-nav">
                        <li class="nav-item toggle" id="toggle-meu-full">
                            <button class="nav-link menu-mobile menu-icon">
                                <div class="bar-menu"></div>
                                <div class="bar-menu"></div>
                                <div class="bar-menu"></div>
                            </button>
                        </li>
                        <!-- <a class="nav-item -menu-mobile ">menu</a> -->
                    </ul>
                </div>
                <section class="section section--menu navbar-nav my-2 my-xl-0 d-none d-xl-block" id="Prospero">
                    <nav class="menu menu--prospero">
                        <ul class="menu__list">
                            <li class="nav-item menu__item menu__item--current">
                                <a href="#home" class="menu__link">Home</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#vantagens" class="menu__link">Vantagens</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#servicos" class="menu__link">Nossos serviços</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#pacotes" class="menu__link">Pacotes</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#contato" class="menu__link">Contato</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#." class="menu__link -phone"><i class="fas fa-phone"></i> 0800 763 3636</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#." target="_BLANK" class="menu__link button-blue">Acessar sistema</a>
                            </li>
                        </ul>
                    </nav>
                </section>
            </div>
        </div>
    </nav>

    <!-- Social Network -->
    <ul class="list-unstyled list-inline social-icons d-none d-xl-block">
        <li class="list-inline-item"><a href="#."><i class="fab fa-facebook-f"></i> Facebook</a></li>
        <li class="list-inline-item"><a href="#."><i class="fab fa-instagram"></i> Instagram</a></li>
        <li class="list-inline-item"><a href="https://api.whatsapp.com/send?phone=51955081075&text=Olá%21%20em%20breve%20m%C3%A1s%20Entrarei%20em%C3%B3n%20Contato." target="_BLANK"><i class="fab fa-whatsapp"></i> Whatsapp</a></li>
    </ul>

    <a href="#vantagens" class="scroll"><img src="assets/images/scroll.png" class="img-fluid"/></a>