<main class="main-content" id="home">
        <section class="slideshow">
            <div class="slideshow-inner">
                <div class="slides">
                    <div class="slide is-active ">
                        <div class="slide-content">
                            <div class="container-fluid h-100">
                                <div class="row h-100 justify-content-center align-items-center">
                                    <div class="col-xl-5 offset-xl-1 align-self-center">
                                        <div class="caption">
                                            <div class="title mb-4">Venha fazer um<br><span>teste de 05 dias e descubra um novo jeito de procurar imóveis</span></div>
                                            <form class="form-test">
                                                <div class="form-group position-relative">
                                                    <input type="email" class="form-control input-slider" placeholder="Insira seu e-mail para começar o teste">
                                                    <button type="button" class="button-gradient btn-test" data-toggle="modal" data-target="#modalTest">Quero fazer meu teste</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 p-0 d-none d-xl-block text-right -slide-image-one">
                                        <img src="assets/images/woman.png" class="img-fluid image-slide" alt="Família Feliz" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="image-container"> 
                            <img src="assets/images/bg-header.jpg" alt="Fisgar" class="image" />
                        </div>
                    </div>

                    <div class="slide">
                        <div class="slide-content">
                            <div class="container-fluid h-100">
                                <div class="row h-100 justify-content-center align-items-center">
                                    <div class="col-xl-5 offset-xl-1 align-self-center">
                                        <div class="caption">
                                            <div class="title mb-4">Venha fazer um<br><span>teste de 05 dias e descubra um novo jeito de procurar imóveis</span></div>
                                            <form class="form-test">
                                                <div class="form-group position-relative">
                                                    <input type="email" class="form-control input-slider" placeholder="Insira seu e-mail para começar o teste">
                                                    <button type="button" class="button-gradient btn-test" data-toggle="modal" data-target="#modalTest">Quero fazer meu teste</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 p-0 d-none d-xl-block text-right">
                                        <img src="assets/images/woman.png" class="img-fluid image-slide" alt="Família Feliz" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="image-container"> 
                            <img src="assets/images/bg-header.jpg" alt="Fisgar" class="image" />
                        </div>
                    </div>

                    <div class="slide">
                        <div class="slide-content">
                            <div class="container-fluid h-100">
                                <div class="row h-100 justify-content-center align-items-center">
                                    <div class="col-xl-5 offset-xl-1 align-self-center">
                                        <div class="caption">
                                            <div class="title mb-4">Venha fazer um<br><span>teste de 05 dias e descubra um novo jeito de procurar imóveis</span></div>
                                            <form class="form-test">
                                                <div class="form-group position-relative">
                                                    <input type="email" class="form-control input-slider" placeholder="Insira seu e-mail para começar o teste">
                                                    <button type="button" class="button-gradient btn-test" data-toggle="modal" data-target="#modalTest">Quero fazer meu teste</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 p-0 d-none d-xl-block text-right">
                                        <img src="assets/images/woman.png" class="img-fluid image-slide" alt="Família Feliz" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="image-container"> 
                            <img src="assets/images/bg-header.jpg" alt="Fisgar" class="image" />
                        </div>
                    </div>
                </div>

                <div class="assets-slide">
                    <div class="pagination d-none d-xl-block">
                        <div class="item is-active"> 
                            <span class="icon">1</span>
                        </div>

                        <div class="item">
                            <span class="icon">2</span>
                        </div>

                        <div class="item">
                            <span class="icon">3</span>
                        </div>
                    </div>

                    <div class="arrows">
                        <div class="arrow prev">
                            <button class="spin circle">
                                <span class="svg svg-arrow-left">
                                    <svg version="1.1" id="svg4-Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14px" height="26px" viewBox="0 0 14 26" enable-background="new 0 0 14 26" xml:space="preserve"> <path d="M13,26c-0.256,0-0.512-0.098-0.707-0.293l-12-12c-0.391-0.391-0.391-1.023,0-1.414l12-12c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414L2.414,13l11.293,11.293c0.391,0.391,0.391,1.023,0,1.414C13.512,25.902,13.256,26,13,26z"/> </svg>
                                    <span class="alt sr-only"></span>
                                </span>
                            </button>
                        </div>

                        <div class="arrow next">
                            <button class="spin circle">
                                <span class="svg svg-arrow-right">
                                    <svg version="1.1" id="svg5-Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14px" height="26px" viewBox="0 0 14 26" enable-background="new 0 0 14 26" xml:space="preserve"> <path d="M1,0c0.256,0,0.512,0.098,0.707,0.293l12,12c0.391,0.391,0.391,1.023,0,1.414l-12,12c-0.391,0.391-1.023,0.391-1.414,0s-0.391-1.023,0-1.414L11.586,13L0.293,1.707c-0.391-0.391-0.391-1.023,0-1.414C0.488,0.098,0.744,0,1,0z"/> </svg>
                                    <span class="alt sr-only"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div> 
        </section>
    </main>