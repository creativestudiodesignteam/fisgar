<?php

require_once '../libs/Mail.php';

if ($_POST) {

    $email[] = array('name' => 'Fisgar', 'email' => 'seu@emailaqui.com');

    $mail = new Mail(
        'email@dedisparo.com',
        'senhadoemail',
        'smtp.dedisparo.com.br',
        587,
        'http://suaurlaqui.com/assets/images/logo.png',
        'Fisgar',
        '#0189f5',
        'tls'
    );
    $mail->fromname = 'Fisgar';
    $mail->recipient = $email;
    $mail->subject = addslashes(utf8_decode('Fisgar | Contato do site!'));
    $mail->body = utf8_decode('
        <tr>
        <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse" align="center">
        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.9; text-align: left;">
        <b>Dados do Contato:</b><br><br>
        <b>Nome:</b> ' . $_POST['name'] . '<br>
        <b>E-mail:</b> ' . $_POST['email'] . '<br>
        <b>Telefone ou Whatsapp:</b> ' . $_POST['phone'] . '<br>
        <b>Mensagem:</b> ' . $_POST['message'] . '<br>
        </div>
        </td>
        </tr>
        <tr>
        <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse;" align="center">
        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center; margin-top: 10px">
        </div>
        </td>
        </tr>
    ');

    $mail->altbody = utf8_decode('Fisgar | Contato do site');

    $send = $mail->send();

    if ($send) {
        $return['response']['mensagem'] = 'Mensagem enviada com sucesso!';
        $return['response']['classe'] = 'alert-success';
        $return['response']['result'] = 'success';
        $return['response']['redirect'] = '/index.php';
    } else {
        $return['response']['mensagem'] = 'Não foi possivel enviar a mensagem!';
        $return['response']['classe'] = 'alert-danger';
        $return['response']['result'] = 'error';
        $return['response']['redirect'] = '';
    }

    print_r(json_encode($return));
    exit();
}
