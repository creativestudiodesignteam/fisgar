<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php include "includes/head.php"?>
  </head>
  <body>

    <?php include "includes/menu.php"?>
    <?php include "includes/menu-overlay.php"?>
    <?php include "includes/slide.php"?>

    <section class="vantagens" id="vantagens">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 text-center wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".3s">
                    <h2 class="title">Quem usa aprova<br><span>Conheça nossas vantagens</span></h2>
                    <div class="elements d-none d-xl-block">
                        <div class="house"><img src="assets/images/house.png" class="img-fluid"/></div>
                        <div class="bar-blue"></div>
                        <div class="bar-one">
                            <div class="-one">
                                <p>Campanhas pelo<br>Whatssap / Email / SMS</p>
                            </div>

                            <div class="-two">
                                <p>Avaliação dos<br>imóveis</p>
                            </div>

                            <div class="-three">
                                <p>Alerta de novas<br>oportunidades<br>do mercado</p>
                            </div>

                            <div class="-four">
                                <p>Captação<br>de dados</p>
                            </div>

                            <div class="-five">
                                 <p>Campanhas pelo<br>Whatssap / Email / SMS</p>
                            </div>

                            <div class="-six">
                                <p>Avaliação dos<br>imóveis</p>
                            </div>

                            <div class="-seven">
                                <p>Alerta de novas<br>oportunidades<br>do mercado</p>
                            </div>

                            <div class="-eight">
                                <p>Captação<br>de dados</p>
                            </div>
                        </div>
                        <div class="bar-two"></div>
                        <div class="bar-three"></div>
                    </div>

                    <div class="vant d-block d-xl-none mt-4">
                        <div class="box">
                            <p>Campanhas pelo Whatssap / Email / SMS</p>
                        </div>

                        <div class="box">
                            <p>Avaliação dos imóveis</p>
                        </div>

                        <div class="box">
                            <p>Alerta de novas oportunidades do mercado</p>
                        </div>

                        <div class="box">
                            <p>Captação de dados</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="servicos" id="servicos">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 offset-xl-2 align-self-center  wow fadeInDown">
                    <h2 class="title mb-3">Uma plataforma<br><span>diferente de tudo que você já usou</span></h2>
                    <a href="#contato" class="button-blue-2 d-inline-block">Entrar em contato</a>
                </div>
                <div class="col-xl-7 align-self-center text-center  wow fadeInDown">
                    <div class="row">
                        <div class="col-xl-3">
                            <div class="demo">
                                <svg viewBox="0 -14 512.00001 512" xmlns="http://www.w3.org/2000/svg"><path d="m136.964844 308.234375c4.78125-2.757813 6.417968-8.878906 3.660156-13.660156-2.761719-4.777344-8.878906-6.417969-13.660156-3.660157-4.78125 2.761719-6.421875 8.882813-3.660156 13.660157 2.757812 4.78125 8.878906 6.421875 13.660156 3.660156zm0 0"/><path d="m95.984375 377.253906 50.359375 87.230469c10.867188 18.84375 35.3125 25.820313 54.644531 14.644531 19.128907-11.054687 25.703125-35.496094 14.636719-54.640625l-30-51.96875 25.980469-15c4.78125-2.765625 6.421875-8.878906 3.660156-13.660156l-13.003906-22.523437c1.550781-.300782 11.746093-2.300782 191.539062-37.570313 22.226563-1.207031 35.542969-25.515625 24.316407-44.949219l-33.234376-57.5625 21.238282-32.167968c2.085937-3.164063 2.210937-7.230469.316406-10.511719l-20-34.640625c-1.894531-3.28125-5.492188-5.203125-9.261719-4.980469l-38.472656 2.308594-36.894531-63.90625c-5.34375-9.257813-14.917969-14.863281-25.605469-14.996094-.128906-.003906-.253906-.003906-.382813-.003906-10.328124 0-19.703124 5.140625-25.257812 13.832031l-130.632812 166.414062-84.925782 49.03125c-33.402344 19.277344-44.972656 62.128907-25.621094 95.621094 17.679688 30.625 54.953126 42.671875 86.601563 30zm102.324219 57.238282c5.523437 9.554687 2.253906 21.78125-7.328125 27.316406-9.613281 5.558594-21.855469 2.144531-27.316407-7.320313l-50-86.613281 34.640626-20c57.867187 100.242188 49.074218 85.011719 50.003906 86.617188zm-22.683594-79.296876-10-17.320312 17.320312-10 10 17.320312zm196.582031-235.910156 13.820313 23.9375-12.324219 18.664063-23.820313-41.261719zm-104.917969-72.132812c2.683594-4.390625 6.941407-4.84375 8.667969-4.796875 1.707031.019531 5.960938.550781 8.527344 4.996093l116.3125 201.464844c3.789063 6.558594-.816406 14.804688-8.414063 14.992188-1.363281.03125-1.992187.277344-5.484374.929687l-123.035157-213.105469c2.582031-3.320312 2.914063-3.640624 3.425781-4.480468zm-16.734374 21.433594 115.597656 200.222656-174.460938 34.21875-53.046875-91.878906zm-223.851563 268.667968c-4.390625-7.597656-6.710937-16.222656-6.710937-24.949218 0-17.835938 9.585937-34.445313 25.011718-43.351563l77.941406-45 50 86.601563-77.941406 45.003906c-23.878906 13.78125-54.515625 5.570312-68.300781-18.304688zm0 0"/><path d="m105.984375 314.574219c-2.761719-4.78125-8.878906-6.421875-13.660156-3.660157l-17.320313 10c-4.773437 2.757813-10.902344 1.113282-13.660156-3.660156-2.761719-4.78125-8.878906-6.421875-13.660156-3.660156s-6.421875 8.878906-3.660156 13.660156c8.230468 14.257813 26.589843 19.285156 40.980468 10.980469l17.320313-10c4.78125-2.761719 6.421875-8.875 3.660156-13.660156zm0 0"/><path d="m497.136719 43.746094-55.722657 31.007812c-4.824218 2.6875-6.5625 8.777344-3.875 13.601563 2.679688 4.820312 8.765626 6.566406 13.601563 3.875l55.71875-31.007813c4.828125-2.6875 6.5625-8.777344 3.875-13.601562-2.683594-4.828125-8.773437-6.5625-13.597656-3.875zm0 0"/><path d="m491.292969 147.316406-38.636719-10.351562c-5.335938-1.429688-10.820312 1.734375-12.25 7.070312-1.429688 5.335938 1.738281 10.816406 7.074219 12.246094l38.640625 10.351562c5.367187 1.441407 10.824218-1.773437 12.246094-7.070312 1.429687-5.335938-1.738282-10.820312-7.074219-12.246094zm0 0"/><path d="m394.199219 7.414062-10.363281 38.640626c-1.429688 5.335937 1.734374 10.816406 7.070312 12.25 5.332031 1.425781 10.816406-1.730469 12.25-7.070313l10.359375-38.640625c1.429687-5.335938-1.734375-10.820312-7.070313-12.25-5.332031-1.429688-10.816406 1.734375-12.246093 7.070312zm0 0"/></svg>
                                <span>+</span>
                                <span class="counter m-0">10</span>
                                <span>milhões</span>
                                <p class="text">de anúncios</p>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="demo">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 295.996 295.996" style="enable-background:new 0 0 295.996 295.996;" xml:space="preserve">
                                    <g>
                                        <path d="M147.998,0C66.392,0,0,66.392,0,147.998s66.392,147.998,147.998,147.998s147.998-66.392,147.998-147.998
                                            S229.605,0,147.998,0z M147.998,279.996c-36.256,0-69.143-14.696-93.022-38.44c-9.536-9.482-17.631-20.41-23.934-32.42
                                            C21.442,190.847,16,170.047,16,147.998C16,75.214,75.214,16,147.998,16c34.523,0,65.987,13.328,89.533,35.102
                                            c12.208,11.288,22.289,24.844,29.558,39.996c8.27,17.239,12.907,36.538,12.907,56.9
                                            C279.996,220.782,220.782,279.996,147.998,279.996z"/>
                                        <circle cx="99.666" cy="114.998" r="16"/>
                                        <circle cx="198.666" cy="114.998" r="16"/>
                                        <path d="M147.715,229.995c30.954,0,60.619-15.83,77.604-42.113l-13.439-8.684c-15.597,24.135-44.126,37.604-72.693,34.308
                                            c-22.262-2.567-42.849-15.393-55.072-34.308l-13.438,8.684c14.79,22.889,39.716,38.409,66.676,41.519
                                            C140.814,229.8,144.27,229.995,147.715,229.995z"/>
                                    </g>
                                </svg>
                                <span>+</span>
                                <span class="counter m-0">500</span>
                                <p class="text">Clientes</p>
                            </div>
                        </div>

                        <div class="col-xl-3">
                            <div class="demo">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill:#fff;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <g>
                                                    <path d="M135.91,462.046c-3.465-20.062-21.001-35.379-42.044-35.379c-16.802,0-31.36,9.762-38.315,23.91
                                                        c-1.374-0.981-2.842-1.852-4.352-2.637V307.2c0-4.71-3.823-8.533-8.533-8.533c-4.71,0-8.533,3.823-8.533,8.533v136.533
                                                        C15.309,443.733,0,459.042,0,477.867S15.309,512,34.133,512H128c14.114,0,25.6-11.486,25.6-25.6
                                                        C153.6,475.042,146.167,465.391,135.91,462.046z M128,494.933H34.133c-9.412,0-17.067-7.654-17.067-17.067
                                                        S24.721,460.8,34.133,460.8c7.962,0,15.036,4.625,17.604,11.52c1.442,3.866,5.436,6.144,9.515,5.41
                                                        c4.062-0.734,7.014-4.267,7.014-8.397c0-14.114,11.486-25.6,25.6-25.6s25.6,11.486,25.6,25.6c0,4.71,3.823,8.533,8.533,8.533
                                                        c4.702,0,8.533,3.831,8.533,8.533S132.702,494.933,128,494.933z"/>
                                                    <path d="M503.467,68.267H341.333c-4.71,0-8.533,3.823-8.533,8.533s3.823,8.533,8.533,8.533h153.6v184.201l-232.9-232.9
                                                        c-1.673-1.673-3.866-2.5-6.059-2.5s-4.386,0.828-6.059,2.5L17.067,269.525V85.333h93.867c4.71,0,8.533-3.823,8.533-8.533V42.667
                                                        c0-4.71-3.823-8.533-8.533-8.533H102.4V17.067h51.2v17.067h-8.533c-4.71,0-8.533,3.823-8.533,8.533V76.8
                                                        c0,4.71,3.823,8.533,8.533,8.533h25.6c4.71,0,8.533-3.823,8.533-8.533s-3.823-8.533-8.533-8.533H153.6V51.2h8.533
                                                        c4.71,0,8.533-3.823,8.533-8.533V8.533c0-4.71-3.823-8.533-8.533-8.533H93.867c-4.71,0-8.533,3.823-8.533,8.533v34.133
                                                        c0,4.71,3.823,8.533,8.533,8.533h8.533v17.067H8.533c-2.261,0-4.437,0.896-6.033,2.5C0.896,72.363,0,74.539,0,76.8l0.034,213.333
                                                        c0,3.456,2.082,6.562,5.274,7.885c1.058,0.435,2.159,0.649,3.26,0.649c2.227,0,4.412-0.87,6.033-2.5L255.974,54.707
                                                        l241.459,241.459c1.63,1.63,3.814,2.5,6.033,2.5c1.101,0,2.21-0.213,3.268-0.649c3.183-1.323,5.265-4.437,5.265-7.885V76.8
                                                        C512,72.09,508.177,68.267,503.467,68.267z"/>
                                                    <path d="M477.867,443.733V307.2c0-4.71-3.823-8.533-8.533-8.533s-8.533,3.823-8.533,8.533v140.74
                                                        c-1.51,0.785-2.978,1.655-4.352,2.637c-6.955-14.148-21.513-23.91-38.315-23.91c-21.043,0-38.579,15.317-42.044,35.379
                                                        c-10.257,3.345-17.69,12.996-17.69,24.354c0,3.012,0.614,5.854,1.579,8.533H213.333c-4.71,0-8.533,3.823-8.533,8.533
                                                        s3.823,8.533,8.533,8.533H384h34.133h59.733C496.691,512,512,496.691,512,477.867S496.691,443.733,477.867,443.733z
                                                        M477.867,494.933h-59.733H384c-4.702,0-8.533-3.831-8.533-8.533s3.831-8.533,8.533-8.533c4.71,0,8.533-3.823,8.533-8.533
                                                        c0-14.114,11.486-25.6,25.6-25.6s25.6,11.486,25.6,25.6c0,4.13,2.953,7.663,7.014,8.397c4.053,0.742,8.073-1.545,9.515-5.41
                                                        c2.568-6.895,9.643-11.52,17.604-11.52c9.412,0,17.067,7.654,17.067,17.067S487.279,494.933,477.867,494.933z"/>
                                                    <path d="M290.133,170.667c0-18.825-15.309-34.133-34.133-34.133c-18.825,0-34.133,15.309-34.133,34.133S237.175,204.8,256,204.8
                                                        C274.825,204.8,290.133,189.491,290.133,170.667z M238.933,170.667c0-9.412,7.654-17.067,17.067-17.067
                                                        c9.412,0,17.067,7.654,17.067,17.067c0,9.412-7.654,17.067-17.067,17.067C246.588,187.733,238.933,180.079,238.933,170.667z"/>
                                                    <path d="M418.133,290.133h-68.267c-4.71,0-8.533,3.823-8.533,8.533v93.867H332.8c-4.71,0-8.533,3.823-8.533,8.533
                                                        s3.823,8.533,8.533,8.533h102.4c4.71,0,8.533-3.823,8.533-8.533s-3.823-8.533-8.533-8.533h-8.533v-93.867
                                                        C426.667,293.956,422.844,290.133,418.133,290.133z M409.6,392.533h-51.2V358.4h51.2V392.533z M409.6,341.333h-51.2V307.2h51.2
                                                        V341.333z"/>
                                                    <path d="M341.333,469.333c0-4.71-3.823-8.533-8.533-8.533h-25.6V298.667c0-4.71-3.823-8.533-8.533-8.533h-85.333
                                                        c-4.71,0-8.533,3.823-8.533,8.533V460.8h-25.6c-4.71,0-8.533,3.823-8.533,8.533v34.133c0,4.71,3.823,8.533,8.533,8.533
                                                        s8.533-3.823,8.533-8.533v-25.6H332.8C337.51,477.867,341.333,474.044,341.333,469.333z M290.133,375.467H281.6
                                                        c-4.71,0-8.533,3.823-8.533,8.533s3.823,8.533,8.533,8.533h8.533V460.8h-68.267V307.2h68.267V375.467z"/>
                                                    <path d="M162.133,290.133H93.867c-4.71,0-8.533,3.823-8.533,8.533v93.867H76.8c-4.71,0-8.533,3.823-8.533,8.533
                                                        S72.09,409.6,76.8,409.6h102.4c4.71,0,8.533-3.823,8.533-8.533s-3.823-8.533-8.533-8.533h-8.533v-93.867
                                                        C170.667,293.956,166.844,290.133,162.133,290.133z M153.6,392.533h-51.2V358.4h51.2V392.533z M153.6,341.333h-51.2V307.2h51.2
                                                        V341.333z"/>
                                                </g>
                                            </g>
                                        </g>
                                </svg>
                                <span>+</span>
                                <span class="counter m-0">3500</span>
                                <p class="text">Imóveis</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="depoimentos">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 offset-xl-2">
                    <h2 class="title mb-3">Veja alguns<br><span>depoimentos de quem usa</span></h2>
                </div>
                <div class="col-xl-7 -aux">
                    <div class="slide-depoimento owl-carousel owl-theme">
                        <div class="item">
                            <div class="box">
                                <div class="row">
                                    <div class="col-xl-2">
                                        <img src="assets/images/quote.png" class="img-fluid quote" />
                                    </div>

                                    <div class="col-xl-10">
                                        <h4 class="name m-0">Felipe Henrique</h4>
                                        <p class="m-0 cargo">Corretor de imóveis</p>

                                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="box">
                                <div class="row">
                                    <div class="col-xl-2">
                                        <img src="assets/images/quote.png" class="img-fluid quote" />
                                    </div>

                                    <div class="col-xl-10">
                                        <h4 class="name m-0">Felipe Henrique</h4>
                                        <p class="m-0 cargo">Corretor de imóveis</p>

                                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="box">
                                <div class="row">
                                    <div class="col-xl-2">
                                        <img src="assets/images/quote.png" class="img-fluid quote" />
                                    </div>

                                    <div class="col-xl-10">
                                        <h4 class="name m-0">Felipe Henrique</h4>
                                        <p class="m-0 cargo">Corretor de imóveis</p>

                                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pacotes" id="pacotes">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center wow fadeInDown">
                    <div class="bar"></div>
                    <h2 class="title">Preços que cabem no seu bolso<br><span>conheça nossos planos</span></h2>
                    <div class="pricing-container">
                        <div class="pricing-switcher mt-4">
                            <p class="fieldset">
                                <input type="radio" name="duration-1" value="monthly" id="monthly-1" checked>
                                <label for="monthly-1" class="mensal">Mensal</label>
                                <input type="radio" name="duration-1" value="yearly" id="yearly-1">
                                <label for="yearly-1" class="anual">Anual</label>
                                <span class="switch"></span>
                            </p>
                        </div>
                        <ul class="pricing-list bounce-invert p-0 mt-4">
                            <li>
                                <ul class="pricing-wrapper">
                                    <li data-type="monthly" class="is-visible">
                                        <header class="pricing-header">
                                            <h2>Básico</h2>
                                            <div class="price">
                                                <span class="currency">R$</span>
                                                <span class="value">150</span>
                                                <span class="duration">mês</span>
                                            </div>
                                        </header>
                                        <div class="pricing-body">
                                            <ul class="pricing-features p-0">
                                                <li>Maximo de 1 usuario</li>
                                                <li>Acceso a 300 dados diarios</li>
                                                <li>Acesso a 20 consultas diarias</li>
                                                <li>Maximo da criacao de 5 filtro personalizado</li>
                                                <li>Relatorio analitico</li>
                                                <li>Radar imobiliario</li>
                                                <li>Filtro de imoveis particulares</li>
                                                <li>Campanhas pelo Whatssap/Email/SMS</li>
                                            </ul>
                                        </div>
                                        <footer class="pricing-footer">
                                            <a class="button-gradient" href="#">Adquirir plano básico</a>
                                        </footer>
                                    </li>
                                    <li data-type="yearly" class="is-hidden">
                                        <header class="pricing-header">
                                            <h2>Básico</h2>
                                            <div class="price">
                                                <span class="currency">R$</span>
                                                <span class="value">190</span>
                                                <span class="duration">ano</span>
                                            </div>
                                        </header>
                                        <div class="pricing-body">
                                            <ul class="pricing-features p-0">
                                                <li>Maximo de 1 usuario</li>
                                                <li>Acceso a 300 dados diarios</li>
                                                <li>Acesso a 20 consultas diarias</li>
                                                <li>Maximo da criacao de 5 filtro personalizado</li>
                                                <li>Relatorio analitico</li>
                                                <li>Radar imobiliario</li>
                                                <li>Filtro de imoveis particulares</li>
                                                <li>Campanhas pelo Whatssap/Email/SMS</li>
                                            </ul>
                                        </div>
                                        <footer class="pricing-footer">
                                            <a class="button-gradient" href="#">Adquirir plano básico</a>
                                        </footer>
                                    </li>
                                </ul>
                            </li>
                            <li class="exclusive">
                                <img src="assets/images/seal.png" class="mais-vendido"/>
                                <ul class="pricing-wrapper">
                                    <li data-type="monthly" class="is-visible">
                                        <header class="pricing-header">
                                            <h2>Exclusivo</h2>
                                            <div class="price">
                                                <span class="currency">R$</span>
                                                <span class="value">350</span>
                                                <span class="duration">mês</span>
                                            </div>
                                        </header>
                                        <div class="pricing-body">
                                            <ul class="pricing-features p-0">
                                                <li>Maximo de 1 usuario</li>
                                                <li>Acceso a 300 dados diarios</li>
                                                <li>Acesso a 20 consultas diarias</li>
                                                <li>Maximo da criacao de 5 filtro personalizado</li>
                                                <li>Relatorio analitico</li>
                                                <li>Radar imobiliario</li>
                                                <li>Filtro de imoveis particulares</li>
                                                <li>Campanhas pelo Whatssap/Email/SMS</li>
                                            </ul>
                                        </div>
                                        <footer class="pricing-footer">
                                            <a class="button-gradient" href="#">Adquirir plano exclusivo</a>
                                        </footer>
                                    </li>
                                    <li data-type="yearly" class="is-hidden">
                                        <header class="pricing-header">
                                            <h2>Exclusivo</h2>
                                            <div class="price">
                                                <span class="currency">R$</span>
                                                <span class="value">550</span>
                                                <span class="duration">ano</span>
                                            </div>
                                        </header>
                                        <div class="pricing-body">
                                            <ul class="pricing-features p-0">
                                                <li>Maximo de 1 usuario</li>
                                                <li>Acceso a 300 dados diarios</li>
                                                <li>Acesso a 20 consultas diarias</li>
                                                <li>Maximo da criacao de 5 filtro personalizado</li>
                                                <li>Relatorio analitico</li>
                                                <li>Radar imobiliario</li>
                                                <li>Filtro de imoveis particulares</li>
                                                <li>Campanhas pelo Whatssap/Email/SMS</li>
                                            </ul>
                                        </div>
                                        <footer class="pricing-footer">
                                            <a class="button-gradient" href="#">Adquirir plano exclusivo</a>
                                        </footer>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <ul class="pricing-wrapper">
                                    <li data-type="monthly" class="is-visible">
                                        <header class="pricing-header">
                                            <h2>Profissonal</h2>
                                            <div class="price">
                                                <span class="currency">R$</span>
                                                <span class="value">450</span>
                                                <span class="duration">mês</span>
                                            </div>
                                        </header>
                                        <div class="pricing-body">
                                            <ul class="pricing-features p-0">
                                                <li>Maximo de 1 usuario</li>
                                                <li>Acceso a 300 dados diarios</li>
                                                <li>Acesso a 20 consultas diarias</li>
                                                <li>Maximo da criacao de 5 filtro personalizado</li>
                                                <li>Relatorio analitico</li>
                                                <li>Radar imobiliario</li>
                                                <li>Filtro de imoveis particulares</li>
                                                <li>Campanhas pelo Whatssap/Email/SMS</li>
                                            </ul>
                                        </div>
                                        <footer class="pricing-footer">
                                            <a class="button-gradient" href="#">Adquirir plano pro</a>
                                        </footer>
                                    </li>
                                    <li data-type="yearly" class="is-hidden">
                                        <header class="pricing-header">
                                            <h2>Profissonal</h2>
                                            <div class="price">
                                                <span class="currency">R$</span>
                                                <span class="value">900</span>
                                                <span class="duration">ano</span>
                                            </div>
                                        </header>
                                        <div class="pricing-body">
                                            <ul class="pricing-features p-0">
                                                <li>Maximo de 1 usuario</li>
                                                <li>Acceso a 300 dados diarios</li>
                                                <li>Acesso a 20 consultas diarias</li>
                                                <li>Maximo da criacao de 5 filtro personalizado</li>
                                                <li>Relatorio analitico</li>
                                                <li>Radar imobiliario</li>
                                                <li>Filtro de imoveis particulares</li>
                                                <li>Campanhas pelo Whatssap/Email/SMS</li>
                                            </ul>
                                        </div>
                                        <footer class="pricing-footer">
                                            <a class="button-gradient" href="#">Adquirir plano pro</a>
                                        </footer>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contato wow fadeInDown" id="contato">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-6">
                    <div class="bar"></div>
                    <h2 class="title">Ainda tem dúvidas?<br><span>entre em contato conosco,<br>atendemos pelo whatsapp</span></h2>
                    <div class="btns mb-3">
                        <a href="https://api.whatsapp.com/send?phone=51955081075&text=Olá%21%20em%20breve%20m%C3%A1s%20Entrarei%20em%C3%B3n%20Contato." target="_BLANK" class="btn-green whats d-inline-block"><i class="fab fa-whatsapp"></i> Conversar pelo whatsapp</a>
                        <a href="#." class="btn-gray info d-inline-block">ou preecha os campos abaixo:</a>
                    </div>
                    <form id="sendmessage" action="action/contact.php">
                        <div class="form-row">
                            <div class="form-group col-xl-12">
                                <input type="text" required class="form-control" name="name" id="inputEmail4" title="Nome" placeholder="Digite seu nome">
                            </div>
                            <div class="form-group col-xl-12">
                                <input type="phone" required class="form-control" name="phone" id="inputPassword4" title="Telefone ou Whatsapp" placeholder="Telefone ou whatsapp">
                            </div>
                            <div class="form-group col-xl-12">
                                <input type="email" required class="form-control" name="email" id="inputPassword4" title="E-mail" placeholder="Digite seu e-mail">
                            </div>
                            <div class="form-group col-xl-12">
                                <textarea class="form-control" required rows="5" name="message" title="Mensagem" placeholder="Mensagem"></textarea>
                            </div>
                        </div>

                        <div class="form-row">
                                <div class="form-group col-xl-12">
                                    <div id="status_response" class="d-none text-center mt-2">
                                        <div class="fa-3x">
                                            <i class="text-dark fas fa-spinner fa-spin"></i>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <div class="row">
                                            <div class="form-group col-xl-12">
                                                <div id="response"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <button type="button" onclick="register('#sendmessage')" class="button-gradient border-0">Enviar mensagem</button>
                    </form>

                    <div class="copy">
                        <p>Desenvolvido por: <strong><a href="https://www.creativedd.com.br/" target="_BLANK">Creative Dev & Design</a></strong></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include "includes/top.php"?>
    <?php include "includes/modal.php"?>
    <?php include "includes/scripts.php"?>
    <script>
        var $doc = $('html, body');
        $('a').click(function() {
            $doc.animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
            }, 500);
            return false;
        });

        $('.counter').countUp();
    </script>

  </body>
</html>